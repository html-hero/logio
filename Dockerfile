FROM node:6.17.1-alpine as builder

RUN apk add --no-cache --virtual .gyp python make g++

USER node

ARG NODE_ENV=production
ENV NODE_ENV=$NODE_ENV

# In case we need global module
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

RUN npm install -g log.io pm2

FROM node:6.17.1-alpine as app

USER node

ARG NODE_ENV=production
ENV NODE_ENV=$NODE_ENV

# In case we need global module
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

ENV APP_DIR=/home/node/app

RUN mkdir $NPM_CONFIG_PREFIX && mkdir $APP_DIR

COPY --from=builder $NPM_CONFIG_PREFIX $NPM_CONFIG_PREFIX

WORKDIR $APP_DIR

COPY ecosystem.config.js ./


COPY conf/log_server.conf conf/web_server.conf /home/node/.log.io/

ENTRYPOINT [ "pm2-runtime", "ecosystem.config.js" ]
