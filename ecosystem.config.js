var runners = [
  {
    name: 'server',
    script: 'log.io-server',
  }
];

if (process.env.HARVESTER === 'true' || process.env.HARVESTER === 'TRUE') {
  runners = [
    {
      name: 'harvester',
      script: 'log.io-harvester',
    }
  ];
}

module.exports = { apps: runners };
